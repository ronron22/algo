for i in  {1..10} ; do echo "$i $i" ; done  > /tmp/test

   nb=0
while read line ; do
   nb=$(((nb+1))) ;
   echo $nb   
done < /tmp/test

linogen() {
lino1=$(shuf -i 1-$nb -n 1)
lino2=$(shuf -i 1-$nb -n 1)
}

while true ; do
  linogen
   if (( $lino1 != $lino2 && $lino1 < ($lino2 + 1) )) ; then
      break
   fi
done

echo "1:$lino1 2:$lino2"

var1=$(sed -n "${lino1}p" /tmp/test)
var2=$(sed -n "${lino2}p" /tmp/test)

#lino2=$((($lino2 - 1)))

sed -i "${lino1}d" /tmp/test
sed -i "${lino2}d" /tmp/test

echo $var1
echo $var2

sed -i "${lino1}i$var2"  /tmp/test
sed -i "${lino2}i$var1"  /tmp/test
#sed -n '10s/var1/var2/'

cat -n /tmp/test
